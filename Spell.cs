﻿using System;
using System.Collections.Generic;
using System.Linq;
using robotManager.Helpful;
using System.Text;
using wManager.Wow;
using wManager.Wow.ObjectManager;
using wManager.Wow.Helpers;
using wManager.Wow.Class;
using System.Windows.Forms;

public static partial class Extensions
{
    private static readonly List<string> AoESpells = new List<string>()
    {
        "Halo",
        "Holy Nova",
        "Power Word: Barrier",
    };

    /*
     *public static float Cooldown(this wManager.Wow.Class.Spell spell)
        {
            return spell.Cooldown();
        }
    */

    public static bool CastSpell ( this Spell spell , WoWUnit unit , bool mustFace = true , bool force = false )
    {
        Logging.WriteDebug( "Casting spell " + spell.NameInGame + " on " + unit.Name );
        Spell currentSpell  = ObjectManager.Me.CastingSpell;
        float castTime      = spell.CastTime;
        bool hasCastTime    = castTime > 0;
        float maxRange      = spell.MaxRange;
        bool alreadyCasting = spell == currentSpell;
        bool casting        = ObjectManager.Me.IsCast;
        if ( force )
            if ( casting )
                Lua.LuaDoString( "SpellStopCasting();" );

        if ( unit != null && unit.IsValid && !unit.IsDead && spell.KnownSpell && spell.IsSpellUsable )
        {
            //	Stop moving if spell needs to be cast
            if ( spell.CastTime > 0 )
            {
                MovementManager.StopMoveTo( false , Usefuls.Latency + 500 );
            }
            //	Cast AoE spells by location
            if ( AoESpells.Contains( spell.Name ) )
            {
                ClickOnTerrain.Spell( spell.Id , unit.Position );
            }
            else
            {
                //	Face unit to cast
                if ( !unit.IsLocalPlayer )
                {
                    Logging.WriteDebug( "Turning to face unit." );
                    MovementManager.Face( unit );
                    var intPtr = wManager.Wow.Memory.WowMemory.Memory.WindowHandle;
                    var relativePoint = intPtr.GetType();
                    //var players = ObjectManager.GetObjectWoWPlayer().Where(p => Lua.LuaDoString<bool>("") );
                    //WoWItem

                    // .PointToClient(Cursor.Position);
                    //Keyboard.PressKey(wManager.Wow.Memory.WowMemory.Memory.WindowHandle, "A");
                    //MovementManager.
                }
                if ( unit.IsLocalPlayer )
                {
                    SpellManager.CastSpellByNameOnSelfLUA( spell.NameInGame );
                }
                if ( unit.IsMyTarget )
                {
                    SpellManager.CastSpellByNameOn( spell.NameInGame , "target" );
                }
                else
                {
                    MemoryRobot.Int128 currentMousePosition = ObjectManager.Me.MouseOverGuid;
                    ObjectManager.Me.MouseOverGuid = unit.Guid;
                    SpellManager.CastSpellByNameOn( spell.NameInGame , "mouseover" );
                    ObjectManager.Me.MouseOverGuid = currentMousePosition;
                    Logging.Write( string.Format( @"Mouseover cast spell ""{0}"" on ""{1}"" (spell=""{0}"", unit=""{1}"", guid=""{2}"")" , spell.NameInGame , unit.Name , unit.Guid ) );
                }
            }
            return true;
        }
        return false;
    }
}