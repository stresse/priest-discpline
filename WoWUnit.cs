﻿using System;
using System.Collections.Generic;
using System.Linq;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;

public static partial class Extensions
{
    //private const string WoWUnitInvalid = "Parameter passed as WoWUnit is not a valid unit (probably null)?";
    public static Spell CurrentSpell => ObjectManager.Me.CastingSpell;

    public static bool Casting ( this WoWUnit unit )
    {
        if ( unit is WoWUnit u )
        {
            return u.IsCast;
        }
        throw new ArgumentException( "Parameter passed as WoWUnit is not a valid unit (probably null)?" );
    }

    public static bool CastingSpell ( this WoWLocalPlayer me , params string[ ] names )
    {
        if ( me is WoWLocalPlayer p )
        {
            names.Any( n => n == me.CastingSpell.Name );
        }
        throw new ArgumentException( "Parameter passed as WoWUnit is not a valid unit (probably null)?" );
    }

    public static bool HasAllBuffs ( this WoWUnit unit , params string[ ] names )
    {
        if ( unit is WoWUnit u )
        {
            foreach ( string name in names )
            {
                if ( !u.HaveBuff( name ) )
                {
                    return false;
                }
            }
            return true;
        }
        throw new ArgumentNullException( nameof( unit ) );
    }

    public static bool HasAnyDebuff ( this WoWUnit unit , params string[ ] names )
    {
        if ( unit is WoWUnit u )
        {
            return names.Any( name => unit.HaveBuff( name ) );
        }
        throw new ArgumentNullException( nameof( unit ) );
    }

    public static bool HasDebuff ( this WoWUnit unit , string name )
    {
        if ( unit is WoWUnit u )
        {
            return u.HaveBuff( name );
        }

        throw new ArgumentException( "Parameter passed as WoWUnit is not a valid unit (probably null)?" );
    }

    public static bool HasDebuffType ( this WoWUnit unit , string type )
    {
        if ( unit == null )
        {
            throw new ArgumentNullException( nameof( unit ) );
        }

        return unit.HaveBuff( type );
    }

    public static bool InLoS ( this WoWUnit unit )
    {
        if ( unit is WoWUnit u )
        {
            return !TraceLine.TraceLineGo( ObjectManager.Me.Position , u.Position , CGWorldFrameHitFlags.HitTestLOS );
        }
        throw new ArgumentNullException( nameof( unit ) );
    }

    /*
    public static bool IsInRange(this WoWUnit unit)
    {
        return unit.IsInRange;
    }
    */

    public static bool IsJoke ( this WoWUnit unit )
    {
        if ( unit is WoWUnit u )
        {
            uint botLevel = ObjectManager.Me.Level;
            uint enemyLevel = u.Level;
            uint levelDifference = botLevel - enemyLevel;
            if ( levelDifference > 20 )
            {
                return true;
            }
            return false;
        }
        throw new Exception( nameof( unit ) );
    }

    public static bool IsPlayer ( this WoWUnit unit )
    {
        if ( unit != null )
        {
            return unit.Type == wManager.Wow.Enums.WoWObjectType.Player; // Player = 6
        }
        throw new ArgumentNullException( nameof( unit ) );
    }

    public static bool IsVillain ( this WoWUnit unit )
    {
        if ( unit == null )
        {
            throw new ArgumentNullException( nameof( unit ) );
        }

        return unit.IsTargetingMeOrMyPetOrPartyMember;
    }

    public static bool IsVillainOrJoke ( this WoWUnit unit )
    {
        if ( unit != null )
        {
            return ( unit.IsJoke() || unit.IsVillain() );
        }

        throw new ArgumentNullException( nameof( unit ) );
    }

    public static bool LevelDifference ( this WoWUnit unit )
    {
        if ( unit is WoWUnit u )
        {
            uint botLevel        = ObjectManager.Me.Level;
            uint enemyLevel      = u.Level;
            uint levelDifference = botLevel - enemyLevel;
            if ( levelDifference > 20 )
            {
                return true;
            }
            return false;
        }
        throw new Exception( nameof( unit ) );
    }

    /*
     * How do I check if a corpse is lootable??
     * public static string Owner(this WoWCorpse unit)
    {
        if (unit.Type == wManager.Wow.Enums.WoWObjectType.Corpse) { return };
            //Memory.WowMemory.Memory.ReadUInt64(unit.GetDescriptorAddress(Descriptors.CorpseFields.Owner));
    }
    */
}