﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using robotManager;
using robotManager.FiniteStateMachine;
using robotManager.Helpful;
using wManager.Wow.Class;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using wManager.Wow.Bot.States;
using Timer = robotManager.Helpful.Timer;
using wManager.Wow.Enums;

public class Main : ICustomClass
{
    private Engine _engine;
    private bool _usePet = false;
    public float Range { get { return 10; } }

    public void Dispose ()
    {
        if ( _engine != null )
        {
            _engine.StopEngine();
            _engine.States.Clear();
        }
    }

    public void Initialize ()
    {
        PriestDisciplineSettings.Load();
        _engine = new Engine( false )
        {
            States = new List<State>
                        {
                             new SpellState(spellName: "Shadow Mend",        priority: 16,  condition: c => ObjectManager.Me.HealthPercent <  75,   castOn: "player"),
                             new SpellState(spellName: "Pain Suppression",   priority: 15,  condition: c => ObjectManager.Me.HealthPercent <  35,   needInView: true, targetFriends: true, aoeSpell: false, aoeSpellMePos: false, oncePerTarget: false, canMoveDuringCast: wManager.Wow.Helpers.FightClassCreator.YesNoAuto.Yes, description: "", castOn: "none", lockFrame: true, waitDuringCasting: false),
                             new SpellState(spellName: "Psychic Scream",     priority: 14,  condition: c => ObjectManager.Me.HealthPercent <= 40),
                             new SpellState(spellName: "Rapture",            priority: 13,  condition: c => ObjectManager.Me.HealthPercent <  40),
                             new SpellState(spellName: "Shadow Word: Pain",  priority: 12,  condition: c => Fight.InFight,                          canMoveDuringCast: wManager.Wow.Helpers.FightClassCreator.YesNoAuto.Yes, waitDuringCasting: false),
                             new SpellState(spellName: "Power Infusion",     priority: 11,  condition: c => Fight.InFight),
                             new SpellState(spellName: "Divine Star",        priority: 10,  condition: c => Fight.InFight),
                             new SpellState(spellName: "Penance",            priority: 9,   condition: c => ObjectManager.Me.HealthPercent <= 65),
                             new SpellState(spellName: "Penance",            priority: 8,   condition: c => Fight.InFight),
                             new SpellState(spellName: "Smite",              priority: 7,   condition: c => Fight.InFight),
                             new SpellState(spellName: "Mindbender",         priority: 6,   condition: c => Fight.InFight),
                             new SpellState(spellName: "Shadow Mend",        priority: 5,   condition: c => ObjectManager.Me.HealthPercent <= 65),
                             new SpellState(spellName: "Shadow Word: Pain",  priority: 4,   condition: c => Fight.InFight),
                             new SpellState(spellName: "Plea",               priority: 3,   condition: c => ObjectManager.Me.CooldownTimeLeft("Atonement") == 0),
                             new SpellState(spellName: "Power Word: Shield", priority: 2,   condition: c => Fight.InFight,                                                                 isBuff: true, inCombatOnly: true, ),
                             new SpellState(spellName: "Power Word: Shield", priority: 1,   condition: c => Fight.InFight && ObjectManager.Me.CooldownTimeLeft("Power Word: Shield") <= 2, castOn:),
                        }
        };

        if ( _usePet )
            _engine.States.Add( new PetManager { Priority = int.MaxValue } );

        _engine.States.Sort();
        _engine.StartEngine( 25 , "_FightClass" , true );
    }

    public void ShowConfiguration ()
    {
        PriestDisciplineSettings.Load();
        PriestDisciplineSettings.CurrentSetting.ToForm();
        PriestDisciplineSettings.CurrentSetting.Save();
    }

    private class PetManager : State
    {
        private readonly Spell _callPet = new Spell("Call Pet 1");
        private readonly Spell _revivePet = new Spell("Revive Pet");
        private bool _petFistTime = true;
        private Timer _petTimer = new Timer(-1);
        public override string DisplayName => "Pet Manager";

        public override bool NeedToRun
        {
            get
            {
                if ( !_petTimer.IsReady )
                    return false;

                if ( ObjectManager.Me.IsDeadMe || ObjectManager.Me.IsMounted || !Conditions.InGameAndConnected )
                {
                    _petFistTime = false;
                    _petTimer = new Timer( 1000 * 3 );
                    return false;
                }
                if ( !ObjectManager.Pet.IsValid || ObjectManager.Pet.IsDead )
                {
                    if ( _petFistTime ) { return true; }
                    else { _petFistTime = true; }
                }
                return false;
            }
        }

        public override void Run ()
        {
            if ( !ObjectManager.Pet.IsValid )
            {
                _callPet.Launch( true );
                Thread.Sleep( Usefuls.Latency + 1000 );
            }
            if ( !ObjectManager.Pet.IsValid || ObjectManager.Pet.IsDead )
                _revivePet.Launch( true );

            _petTimer = new Timer( 1000 * 2 );
        }
    }

    public class FightState : State
    {
        public FightState ( int priority , string displayName )
        {
            Priority = priority;
            DisplayName = displayName ?? throw new ArgumentNullException( nameof( displayName ) );
        }

        public override List<State> BeforeStates => base.BeforeStates;
        public override string DisplayName { get => base.DisplayName; set => base.DisplayName = value; }
        public override bool NeedToRun => throw new NotImplementedException();
        public override List<State> NextStates => base.NextStates;
        public sealed override int Priority { get => base.Priority; set => base.Priority = value; }

        public override void Run () => throw new NotImplementedException();

        public override string ToString () => "FightState > " + DisplayName;
    }
}

[Serializable]
public class PriestDisciplineSettings : Settings
{
    private PriestDisciplineSettings ()
    {
        ConfigWinForm( new System.Drawing.Point( 400 , 400 ) , "DiscPriesta " + Translate.Get( "Settings" ) );
    }

    public static PriestDisciplineSettings CurrentSetting { get; set; }

    public static bool Load ()
    {
        try
        {
            if ( File.Exists( AdviserFilePathAndName( "Priest-Discipline" , ObjectManager.Me.Name + "." + Usefuls.RealmName ) ) )
            {
                CurrentSetting = Load<PriestDisciplineSettings>( AdviserFilePathAndName( "Priest-Discipline" , ObjectManager.Me.Name + "." + Usefuls.RealmName ) );
                return true;
            }
            CurrentSetting = new PriestDisciplineSettings();
        }
        catch ( Exception e )
        {
            Logging.WriteError( "PriestDisciplineSettings > Load(): " + e );
        }
        return false;
    }

    public bool Save ()
    {
        try
        {
            return Save( AdviserFilePathAndName( "Priest-Discipline" , ObjectManager.Me.Name + "." + Usefuls.RealmName ) );
        }
        catch ( Exception e )
        {
            Logging.WriteError( "PriestDisciplineSettings > Save(): " + e );
            return false;
        }
    }
}

/*
 * SETTINGS
*/